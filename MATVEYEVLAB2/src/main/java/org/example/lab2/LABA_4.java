package org.example.lab2;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class LABA_4 {
    private static final String URL_Postman = "https://de28b35f-4ee7-497a-9b52-a48fe4a5fce8.mock.pstmn.io";
    private static final String GET_OK = "/ownerName/success";
    private static final String GET_notOK = "/ownerName/unsuccess";
    private static final String POST_OK = "/createSomething?premission=yes";
    private static final String POST_notOK = "/createSomething";
    private static final String PUT = "/updateMe";
    private static final String DELETE = "/deleteWorld";
    private static final String SessionID = "123456789";

    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = URL_Postman;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test()
    public void GET_OK(){
        Response response = given()
                .get(GET_OK);

        System.out.println("\nТест GET (успішний). Код: " + response.statusCode() + ". Ім'я = " + response.jsonPath().get("name"));
    }
    @Test(dependsOnMethods = "GET_OK")
    public void GET_notOK(){
        Response response = given()
                .get(GET_notOK);

        System.out.println("\nТест GET (неуспішний). Код: " + response.statusCode() + ". Виключення = " + response.jsonPath().get("exception"));
    }

    @Test(dependsOnMethods = "GET_notOK")
    public void POST_OK(){

        Response response = given()
                .post(POST_OK);

        System.out.println("\nТест POST (успішний). Код: " + response.statusCode() + ". Результат = " + response.jsonPath().get("result"));
    }

    @Test(dependsOnMethods = "POST_OK")
    public void POST_notOK(){

        Response response = given()
                .post(POST_notOK);

        System.out.println("\nТест POST (неуспішний). Код: " + response.statusCode() + ". Результат = " + response.jsonPath().get("result"));
    }

    @Test(dependsOnMethods = "POST_notOK")
    public void PUT(){

        Response response =  given()
                .post(PUT);

        System.out.println("\nТест PUT. Код: " + response.statusCode());
    }

    @Test(dependsOnMethods = "PUT")
    public void DELETE(){
        Response response = given()
                .header("SessionID", SessionID)
                .delete(DELETE);
        response.then()
                .statusCode(HttpStatus.SC_GONE);

        System.out.println("\nТест DELETE. Код: " + response.statusCode() + ". Видалено (0) = " + response.jsonPath().get("world"));
    }
}
