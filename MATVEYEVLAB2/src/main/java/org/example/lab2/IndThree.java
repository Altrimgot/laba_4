package org.example.lab2;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.hc.core5.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class IndThree {
    private static final String baseUrl = "https://petstore.swagger.io/v2";

    private static final String PET = "/pet";

    private String petId;
    private String petName;

    private String tagName = "MatveyevY";
    private String category = "122-20sk-1.20";

    private String stat = "доступний";

    private static final String STORE = "/store/order",
            STORE_ORDER_GET = STORE + "/{orderId}";

    private String orderId;

    String JsonBoduPet(){
        return  " {\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"" + category + "\"\n" +
                "  },\n" +
                "  \"name\": \"" + Faker.instance().dog().name() + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"" + Faker.instance().dog().memePhrase() + "\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"" + tagName + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \""+ stat +"\"\n" +
                "}";
    }

    String JsonBoduPetWithId(String petId){
        return  " {\n" +
                " \"id\":"  + petId + "," +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"" + category + "\"\n" +
                "  },\n" +
                "  \"name\": \"" + tagName + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"" + Faker.instance().dog().memePhrase() + "\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"" + tagName + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \""+ stat +"\"\n" +
                "}";
    }

    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    //ТЕСТ ДОДАВАННЯ ADD
    @Test
    public void vAddPet(){

        Response response = given()
                .contentType(ContentType.JSON).body(JsonBoduPet())
                .post(PET);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        petId = response.jsonPath().get("id").toString();
        petName = response.jsonPath().get("name").toString();

        System.out.println("1. Додано тварину " + petId + " з ім'ям " + petName + ". Категорія "
                + response.jsonPath().get("category.name").toString()
                + ". Тег " + response.jsonPath().get("tags.name").toString()
                + ". Статус " + response.jsonPath().get("status").toString());
    }

    //ТЕСТ ОНОВЛЕННЯ PUT
    @Test(dependsOnMethods = "vAddPet")
    public void vPutPet(){

        category = "122-20sk-1.20";

        stat = "очікування";

        Response response = given().contentType(ContentType.JSON).body(JsonBoduPetWithId(petId))
                .put(PET);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        System.out.println("2. Оновлена тварина " + petId + " з ім'ям " + petName + ". Категорія "
                + response.jsonPath().get("category.name").toString()
                + ". Тег " + response.jsonPath().get("tags.name").toString()
                + ". Статус " + response.jsonPath().get("status").toString());
    }

    //ТЕСТ ДОДАВАННЯ ЗАМОВЛЕННЯ ADD
    @Test(dependsOnMethods = "vPutPet")
    public void vAddOrder(){
        Map<String, ?> body = Map.of(
                "petId", petId,
                "quantity", Integer.valueOf("5"),
                "shipDate", "2023-03-10T18:23:17.682Z",
                "status", "додано",
                "complete", Boolean.valueOf("true")
        );

        Response response = given()
                .body(body)
                .post(STORE);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        orderId = response.jsonPath().get("id").toString();

        System.out.println("3. Ідентифікатор замовлення " + orderId
                + ". Замовлення на тварину " + response.jsonPath().get("petId").toString());
    }

    //ТЕСТ ОТРИМАННЯ ІНФОРМАЦІЇ О ЗАМОВЛЕННІ GET
    @Test(dependsOnMethods = "vAddOrder")
    public void vGetOrder(){

        Response response = given().pathParams("orderId", orderId)
                .get(STORE_ORDER_GET);

        response.then()
                .statusCode(org.apache.http.HttpStatus.SC_OK);

        System.out.println("4. Пошук інформації про " + response.jsonPath().get("id").toString()
                + ". ID тварини "
                + response.jsonPath().get("petId").toString()
                + ". Кількість " + response.jsonPath().get("quantity").toString()
                + ". Дата " + response.jsonPath().get("shipDate").toString()
                + ". Статус " + response.jsonPath().get("status").toString());
    }

    //ТЕСТ ВИДАЛЕННЯ ЗАМОВЛЕННЯ DELETE
    @Test(dependsOnMethods = "vGetOrder")
    public void vDeleteOrder(){

        Response response = given().pathParams("orderId", orderId)
                .delete(STORE_ORDER_GET);

        response.then()
                .statusCode(org.apache.http.HttpStatus.SC_OK);

        System.out.println("5. Замовлення видалено " + response.statusCode());
    }
}

